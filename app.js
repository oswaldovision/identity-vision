require('./config/config')
require('./db/mongoose')

const express = require('express')
const cors = require('cors')
const bodyparser = require('body-parser')

const userRoute = require('./route/user')
const port = process.env.PORT

const app = express()
app.use(bodyparser.json())
app.use(cors())

app.use('/v1/user/',userRoute)

app.listen(port, () => {
  console.log(`Identity server listening on port: ${port}`);
});


module.exports = {app};